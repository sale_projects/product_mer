import React from 'react';
import Subscribe from './components/Subscribe';
import Header from './components/Header';

class HomePage extends React.Component {
    render() {
        return (
            <div>
                <Header/>
                <div className="ps-panel--sidebar" id="cart-mobile">
                    <div className="ps-panel__header">
                        <h3>Shopping Cart</h3>
                    </div>
                    <div className="navigation__content">
                        <div className="ps-cart--mobile">
                            <div className="ps-cart__content">
                                <div className="ps-cart__items">
                                    <div className="ps-product--mini-cart">
                                        <div className="ps-product__thumbnail"><a href="#"><img src="template/user_chikery/img/product/12.png" alt="" /></a></div>
                                        <div className="ps-product__content"><span className="ps-btn--close" /><a className="ps-product__title" href="product-default.html">Jean Woman Summer</a>
                                            <p><strong>Quantity: 1</strong></p><small>$12.00</small>
                                        </div>
                                    </div>
                                    <div className="ps-product--mini-cart">
                                        <div className="ps-product__thumbnail"><a href="#"><img src="template/user_chikery/img/product/13.png" alt="" /></a></div>
                                        <div className="ps-product__content"><span className="ps-btn--close" /><a className="ps-product__title" href="product-default.html">Jean Woman Summer</a>
                                            <p><strong>Quantity: 1</strong></p><small>$12.00</small>
                                        </div>
                                    </div>
                                </div>
                                <div className="ps-cart__footer">
                                    <h3>Sub Total:<strong>$48.00</strong></h3>
                                    <figure><a className="ps-btn" href="shopping-cart.html">View Cart</a><a className="ps-btn ps-btn--dark" href="checkout.html">Checkout</a></figure>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="ps-panel--sidebar" id="navigation-mobile">
                    <div className="ps-panel__header">
                        <h3>Menu</h3>
                    </div>
                    <div className="ps-panel__content">
                        <ul className="menu--mobile">
                            <li className="current-menu-item menu-item-has-children"><a href="#">Home</a><span className="sub-toggle" />
                                <ul className="sub-menu">
                                    <li><a href="#">Homepage 1</a>
                                    </li>
                                    <li><a href="homepage-2.html">Homepage 2</a>
                                    </li>
                                    <li><a href="homepage-3.html">Homepage 3</a>
                                    </li>
                                    <li><a href="homepage-4.html">Homepage 4</a>
                                    </li>
                                    <li><a href="homepage-5.html">Homepage 5</a>
                                    </li>
                                    <li><a href="homepage-6.html">Homepage 6</a>
                                    </li>
                                </ul>
                            </li>
                            <li className="menu-item-has-children"><a href="shop-default.html">Shop</a><span className="sub-toggle" />
                                <ul className="sub-menu">
                                    <li><a href="shop-default.html">Shop Default</a>
                                    </li>
                                    <li><a href="shop-fullwidth.html">Shop Fullwidth</a>
                                    </li>
                                    <li><a href="shop-fullwidth-no-sidebar.html">Shop Fullwidth No Sidebar</a>
                                    </li>
                                    <li><a href="shop-no-sidebar.html">Shop No Sidebar</a>
                                    </li>
                                    <li><a href="shop-sidebar-right.html">Shop Sidebar Right</a>
                                    </li>
                                    <li><a href="shop-horizontal.html">Shop Horizontal</a>
                                    </li>
                                    <li className="menu-item-has-children"><a href="product-default.html">Product</a><span className="sub-toggle"><i className="fa fa-angle-down" /></span>
                                        <ul className="sub-menu">
                                            <li><a href="product-default.html">Product Default</a>
                                            </li>
                                            <li><a href="product-background.html">Product Background</a>
                                            </li>
                                            <li><a href="product-background-center.html">Product Background Center</a>
                                            </li>
                                            <li><a href="product-background-center-2.html">Product Background Center 2</a>
                                            </li>
                                            <li><a href="product-sticky.html">Product Sticky</a>
                                            </li>
                                            <li><a href="product-thumbnail-left.html">Product Thumbnail Left</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="about-us.html">About</a>
                            </li>
                            <li className="current-menu-item menu-item-has-children"><a href="about-us.html">Pages</a><span className="sub-toggle" />
                                <ul className="sub-menu">
                                    <li><a href="about-us.html">About</a>
                                    </li>
                                    <li><a href="homepage-2.html">Checkout</a>
                                    </li>
                                    <li><a href="whishlist.html">Whishlist</a>
                                    </li>
                                    <li><a href="homepage-4.html">Compare</a>
                                    </li>
                                </ul>
                            </li>
                            <li className="menu-item-has-children"><a href="blog-grid.html">News</a><span className="sub-toggle" />
                                <ul className="sub-menu">
                                    <li><a href="blog-grid.html">Blog grid</a>
                                    </li>
                                    <li><a href="blog-grid-full-width.html">Blog grid fullwidth</a>
                                    </li>
                                    <li><a href="blog-list.html">Blog List</a>
                                    </li>
                                    <li><a href="blog-detail.html">Blog Detail</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="contact-us.html">Contact</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div className="navigation--list">
                    <div className="navigation__content"><a className="navigation__item active" href="#"><i className="fa fa-home" /></a><a className="navigation__item ps-toggle--sidebar" href="#navigation-mobile"><i className="fa fa-bars" /></a><a className="navigation__item ps-toggle--sidebar" href="#search-sidebar"><i className="fa fa-search" /></a><a className="navigation__item ps-toggle--sidebar" href="#cart-mobile"><i className="fa fa-shopping-basket" /></a></div>
                </div>
                {/*include search-sidebar*/}
                <div id="homepage-1">
                    <div className="ps-home-banner">
                        <div className="ps-carousel--dots owl-slider" data-owl-auto="true" data-owl-loop="true" data-owl-speed={5000} data-owl-gap={0} data-owl-nav="true" data-owl-dots="true" data-owl-item={1} data-owl-item-xs={1} data-owl-item-sm={1} data-owl-item-md={1} data-owl-item-lg={1} data-owl-duration={1000} data-owl-mousedrag="off" data-owl-animate-in="fadeIn" data-owl-animate-out="fadeOut">
                            <div className="ps-banner ps-banner--1 bg--cover" data-background="template/user_chikery/img/banner/home-1/1.jpg">
                                <div className="ps-banner__content">
                                    <h3>Super Delicious Cake</h3>
                                    <p>100% Natural, FRESH baked goods</p><a className="ps-btn" href="#"> Book Now</a>
                                </div>
                            </div>
                            <div className="ps-banner ps-banner--1 bg--cover" data-background="template/user_chikery/img/banner/home-1/2.jpg">
                                <div className="ps-banner__content">
                                    <h3>Super Delicious Cake</h3>
                                    <p>100% Natural, FRESH baked goods</p><a className="ps-btn" href="#"> Book Now</a>
                                </div>
                            </div>
                            <div className="ps-banner ps-banner--1 bg--cover" data-background="template/user_chikery/img/banner/home-1/3.jpg">
                                <div className="ps-banner__content">
                                    <h3>Super Delicious Cake</h3>
                                    <p>100% Natural, FRESH baked goods</p><a className="ps-btn" href="#"> Book Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="ps-section ps-home-about bg--cover" data-background="template/user_chikery/img/bg/home-about.jpg">
                        <div className="container">
                            <div className="row">
                                <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                                    <div className="ps-section__image"><img src="template/user_chikery/img/homepage/home-1/home-about.png" alt="" /></div>
                                </div>
                                <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                                    <div className="ps-section__header">
                                        <p>WELCOME TO CHIKERY</p>
                                        <h3>Chikery Cake History</h3><i className="chikery-tt3" />
                                    </div>
                                    <div className="ps-section__content">
                                        <p>“ Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sed turpis feugiat, mollis felis vel, viverra metus. Sed vel nulla non neque dictum imperdiet hendrerit ”</p>
                                        <div className="ps-section__image"><img src="template/user_chikery/img/homepage/home-1/signature.png" alt="" /></div>
                                        <h5><span>Marry Lulie</span> - Ceo Chikery</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="ps-section ps-home-best-services">
                        <div className="ps-section__left bg--cover" data-background="template/user_chikery/img/bg/home-1/home-best-services.jpg" />
                        <div className="ps-section__right">
                            <div className="ps-section__container">
                                <div className="ps-section__header">
                                    <p>CHIKERY CAKE</p>
                                    <h3>Best Services</h3><i className="chikery-tt3" />
                                </div>
                                <div className="ps-section__content">
                                    <p>Consectetur adipiscing elit. Curabitur sed turpis feugiat, sed vel nulla non neque. Nullamlacinia faucibus risus, a euismod lorem tincidunt id. Vestibulum imperdiet nibh vel magna lacinia ultricesimperdiet.</p>
                                    <ul>
                                        <li>BEST QUALITY</li>
                                        <li>FAST DELIVERED</li>
                                        <li>Event Cakes</li>
                                        <li>INGREDIENT SUPPLY</li>
                                        <li>ONLINE BOOKING</li>
                                    </ul>
                                </div>
                            </div>
                            <div className="ps-section__image bg--cover" data-background="template/user_chikery/img/bg/home-1/home-best-services-2.jpg" />
                        </div>
                    </div>
                    <div className="ps-section ps-home-menu-of-day">
                        <div className="ps-section__left">
                            <div className="ps-section__header">
                                <p>Special Menu</p>
                                <h3>Menu of the day</h3><i className="chikery-tt3" />
                            </div>
                            <div className="ps-section__content">
                                <div className="ps-block--product-menu ps-tab-root">
                                    <div className="ps-block__header">
                                        <ul className="ps-tab-list">
                                            <li className="active"><a href="#tab-menu-1"><i className="chikery-bk1" /> Bread</a></li>
                                            <li><a href="#tab-menu-2"><i className="chikery-bk2" /> Bakery</a></li>
                                            <li><a href="#tab-menu-3"><i className="chikery-bk3" /> Buchanan</a></li>
                                            <li><a href="#tab-menu-4"><i className="chikery-bk4" /> Cookies</a></li>
                                        </ul>
                                    </div>
                                    <div className="ps-tabs">
                                        <div className="ps-tab active" id="tab-menu-1">
                                            <div className="row">
                                                <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                                                    <div className="ps-product--horizontal">
                                                        <div className="ps-product__thumbnail"><a className="ps-product__overlay" href="product-default.html" /><img src="template/user_chikery/img/product/2.png" alt="" /></div>
                                                        <div className="ps-product__container">
                                                            <div className="ps-product__content"><a className="ps-product__title" href="product-default.html">Red sugar flower</a>
                                                                <p>Nam ac elit a ante commodo tristique</p>
                                                            </div>
                                                            <div className="ps-product__price"><span>$12.00</span></div>
                                                        </div>
                                                    </div>
                                                    <div className="ps-product--horizontal">
                                                        <div className="ps-product__thumbnail"><a className="ps-product__overlay" href="product-default.html" /><img src="template/user_chikery/img/product/41.png" alt="" /></div>
                                                        <div className="ps-product__container">
                                                            <div className="ps-product__content"><a className="ps-product__title" href="product-default.html">Red sugar flower</a>
                                                                <p>Nam ac elit a ante commodo tristique</p>
                                                            </div>
                                                            <div className="ps-product__price"><span>$2.00</span></div>
                                                        </div>
                                                    </div>
                                                    <div className="ps-product--horizontal">
                                                        <div className="ps-product__thumbnail"><a className="ps-product__overlay" href="product-default.html" /><img src="template/user_chikery/img/product/9.png" alt="" /></div>
                                                        <div className="ps-product__container">
                                                            <div className="ps-product__content"><a className="ps-product__title" href="product-default.html">Red sugar flower</a>
                                                                <p>Nam ac elit a ante commodo tristique</p>
                                                            </div>
                                                            <div className="ps-product__price"><span>$6.00</span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                                                    <div className="ps-product--horizontal">
                                                        <div className="ps-product__thumbnail"><a className="ps-product__overlay" href="product-default.html" /><img src="template/user_chikery/img/product/23.png" alt="" /></div>
                                                        <div className="ps-product__container">
                                                            <div className="ps-product__content"><a className="ps-product__title" href="product-default.html">Red sugar flower</a>
                                                                <p>Nam ac elit a ante commodo tristique</p>
                                                            </div>
                                                            <div className="ps-product__price"><span>$12.00</span></div>
                                                        </div>
                                                    </div>
                                                    <div className="ps-product--horizontal">
                                                        <div className="ps-product__thumbnail"><a className="ps-product__overlay" href="product-default.html" /><img src="template/user_chikery/img/product/3.png" alt="" /></div>
                                                        <div className="ps-product__container">
                                                            <div className="ps-product__content"><a className="ps-product__title" href="product-default.html">Red sugar flower</a>
                                                                <p>Nam ac elit a ante commodo tristique</p>
                                                            </div>
                                                            <div className="ps-product__price"><span>$2.00</span></div>
                                                        </div>
                                                    </div>
                                                    <div className="ps-product--horizontal">
                                                        <div className="ps-product__thumbnail"><a className="ps-product__overlay" href="product-default.html" /><img src="template/user_chikery/img/product/9.png" alt="" /></div>
                                                        <div className="ps-product__container">
                                                            <div className="ps-product__content"><a className="ps-product__title" href="product-default.html">Red sugar flower</a>
                                                                <p>Nam ac elit a ante commodo tristique</p>
                                                            </div>
                                                            <div className="ps-product__price"><span>$5.00</span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="ps-tab" id="tab-menu-2">
                                            <div className="row">
                                                <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                                                    <div className="ps-product--horizontal">
                                                        <div className="ps-product__thumbnail"><a className="ps-product__overlay" href="product-default.html" /><img src="template/user_chikery/img/product/1.png" alt="" /></div>
                                                        <div className="ps-product__container">
                                                            <div className="ps-product__content"><a className="ps-product__title" href="product-default.html">Red sugar flower</a>
                                                                <p>Nam ac elit a ante commodo tristique</p>
                                                            </div>
                                                            <div className="ps-product__price"><span>$12.00</span></div>
                                                        </div>
                                                    </div>
                                                    <div className="ps-product--horizontal">
                                                        <div className="ps-product__thumbnail"><a className="ps-product__overlay" href="product-default.html" /><img src="template/user_chikery/img/product/2.png" alt="" /></div>
                                                        <div className="ps-product__container">
                                                            <div className="ps-product__content"><a className="ps-product__title" href="product-default.html">Red sugar flower</a>
                                                                <p>Nam ac elit a ante commodo tristique</p>
                                                            </div>
                                                            <div className="ps-product__price"><span>$2.00</span></div>
                                                        </div>
                                                    </div>
                                                    <div className="ps-product--horizontal">
                                                        <div className="ps-product__thumbnail"><a className="ps-product__overlay" href="product-default.html" /><img src="template/user_chikery/img/product/3.png" alt="" /></div>
                                                        <div className="ps-product__container">
                                                            <div className="ps-product__content"><a className="ps-product__title" href="product-default.html">Red sugar flower</a>
                                                                <p>Nam ac elit a ante commodo tristique</p>
                                                            </div>
                                                            <div className="ps-product__price"><span>$6.00</span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                                                    <div className="ps-product--horizontal">
                                                        <div className="ps-product__thumbnail"><a className="ps-product__overlay" href="product-default.html" /><img src="template/user_chikery/img/product/4.png" alt="" /></div>
                                                        <div className="ps-product__container">
                                                            <div className="ps-product__content"><a className="ps-product__title" href="product-default.html">Red sugar flower</a>
                                                                <p>Nam ac elit a ante commodo tristique</p>
                                                            </div>
                                                            <div className="ps-product__price"><span>$12.00</span></div>
                                                        </div>
                                                    </div>
                                                    <div className="ps-product--horizontal">
                                                        <div className="ps-product__thumbnail"><a className="ps-product__overlay" href="product-default.html" /><img src="template/user_chikery/img/product/5.png" alt="" /></div>
                                                        <div className="ps-product__container">
                                                            <div className="ps-product__content"><a className="ps-product__title" href="product-default.html">Red sugar flower</a>
                                                                <p>Nam ac elit a ante commodo tristique</p>
                                                            </div>
                                                            <div className="ps-product__price"><span>$2.00</span></div>
                                                        </div>
                                                    </div>
                                                    <div className="ps-product--horizontal">
                                                        <div className="ps-product__thumbnail"><a className="ps-product__overlay" href="product-default.html" /><img src="template/user_chikery/img/product/6.png" alt="" /></div>
                                                        <div className="ps-product__container">
                                                            <div className="ps-product__content"><a className="ps-product__title" href="product-default.html">Red sugar flower</a>
                                                                <p>Nam ac elit a ante commodo tristique</p>
                                                            </div>
                                                            <div className="ps-product__price"><span>$5.00</span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="ps-tab" id="tab-menu-3">
                                            <div className="row">
                                                <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                                                    <div className="ps-product--horizontal">
                                                        <div className="ps-product__thumbnail"><a className="ps-product__overlay" href="product-default.html" /><img src="template/user_chikery/img/product/7.png" alt="" /></div>
                                                        <div className="ps-product__container">
                                                            <div className="ps-product__content"><a className="ps-product__title" href="product-default.html">Red sugar flower</a>
                                                                <p>Nam ac elit a ante commodo tristique</p>
                                                            </div>
                                                            <div className="ps-product__price"><span>$12.00</span></div>
                                                        </div>
                                                    </div>
                                                    <div className="ps-product--horizontal">
                                                        <div className="ps-product__thumbnail"><a className="ps-product__overlay" href="product-default.html" /><img src="template/user_chikery/img/product/8.png" alt="" /></div>
                                                        <div className="ps-product__container">
                                                            <div className="ps-product__content"><a className="ps-product__title" href="product-default.html">Red sugar flower</a>
                                                                <p>Nam ac elit a ante commodo tristique</p>
                                                            </div>
                                                            <div className="ps-product__price"><span>$2.00</span></div>
                                                        </div>
                                                    </div>
                                                    <div className="ps-product--horizontal">
                                                        <div className="ps-product__thumbnail"><a className="ps-product__overlay" href="product-default.html" /><img src="template/user_chikery/img/product/9.png" alt="" /></div>
                                                        <div className="ps-product__container">
                                                            <div className="ps-product__content"><a className="ps-product__title" href="product-default.html">Red sugar flower</a>
                                                                <p>Nam ac elit a ante commodo tristique</p>
                                                            </div>
                                                            <div className="ps-product__price"><span>$6.00</span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                                                    <div className="ps-product--horizontal">
                                                        <div className="ps-product__thumbnail"><a className="ps-product__overlay" href="product-default.html" /><img src="template/user_chikery/img/product/21.png" alt="" /></div>
                                                        <div className="ps-product__container">
                                                            <div className="ps-product__content"><a className="ps-product__title" href="product-default.html">Red sugar flower</a>
                                                                <p>Nam ac elit a ante commodo tristique</p>
                                                            </div>
                                                            <div className="ps-product__price"><span>$12.00</span></div>
                                                        </div>
                                                    </div>
                                                    <div className="ps-product--horizontal">
                                                        <div className="ps-product__thumbnail"><a className="ps-product__overlay" href="product-default.html" /><img src="template/user_chikery/img/product/11.png" alt="" /></div>
                                                        <div className="ps-product__container">
                                                            <div className="ps-product__content"><a className="ps-product__title" href="product-default.html">Red sugar flower</a>
                                                                <p>Nam ac elit a ante commodo tristique</p>
                                                            </div>
                                                            <div className="ps-product__price"><span>$2.00</span></div>
                                                        </div>
                                                    </div>
                                                    <div className="ps-product--horizontal">
                                                        <div className="ps-product__thumbnail"><a className="ps-product__overlay" href="product-default.html" /><img src="template/user_chikery/img/product/12.png" alt="" /></div>
                                                        <div className="ps-product__container">
                                                            <div className="ps-product__content"><a className="ps-product__title" href="product-default.html">Red sugar flower</a>
                                                                <p>Nam ac elit a ante commodo tristique</p>
                                                            </div>
                                                            <div className="ps-product__price"><span>$5.00</span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="ps-tab" id="tab-menu-4">
                                            <div className="row">
                                                <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                                                    <div className="ps-product--horizontal">
                                                        <div className="ps-product__thumbnail"><a className="ps-product__overlay" href="product-default.html" /><img src="template/user_chikery/img/product/13.png" alt="" /></div>
                                                        <div className="ps-product__container">
                                                            <div className="ps-product__content"><a className="ps-product__title" href="product-default.html">Red sugar flower</a>
                                                                <p>Nam ac elit a ante commodo tristique</p>
                                                            </div>
                                                            <div className="ps-product__price"><span>$12.00</span></div>
                                                        </div>
                                                    </div>
                                                    <div className="ps-product--horizontal">
                                                        <div className="ps-product__thumbnail"><a className="ps-product__overlay" href="product-default.html" /><img src="template/user_chikery/img/product/14.png" alt="" /></div>
                                                        <div className="ps-product__container">
                                                            <div className="ps-product__content"><a className="ps-product__title" href="product-default.html">Red sugar flower</a>
                                                                <p>Nam ac elit a ante commodo tristique</p>
                                                            </div>
                                                            <div className="ps-product__price"><span>$2.00</span></div>
                                                        </div>
                                                    </div>
                                                    <div className="ps-product--horizontal">
                                                        <div className="ps-product__thumbnail"><a className="ps-product__overlay" href="product-default.html" /><img src="template/user_chikery/img/product/15.png" alt="" /></div>
                                                        <div className="ps-product__container">
                                                            <div className="ps-product__content"><a className="ps-product__title" href="product-default.html">Red sugar flower</a>
                                                                <p>Nam ac elit a ante commodo tristique</p>
                                                            </div>
                                                            <div className="ps-product__price"><span>$6.00</span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                                                    <div className="ps-product--horizontal">
                                                        <div className="ps-product__thumbnail"><a className="ps-product__overlay" href="product-default.html" /><img src="template/user_chikery/img/product/16.png" alt="" /></div>
                                                        <div className="ps-product__container">
                                                            <div className="ps-product__content"><a className="ps-product__title" href="product-default.html">Red sugar flower</a>
                                                                <p>Nam ac elit a ante commodo tristique</p>
                                                            </div>
                                                            <div className="ps-product__price"><span>$12.00</span></div>
                                                        </div>
                                                    </div>
                                                    <div className="ps-product--horizontal">
                                                        <div className="ps-product__thumbnail"><a className="ps-product__overlay" href="product-default.html" /><img src="template/user_chikery/img/product/17.png" alt="" /></div>
                                                        <div className="ps-product__container">
                                                            <div className="ps-product__content"><a className="ps-product__title" href="product-default.html">Red sugar flower</a>
                                                                <p>Nam ac elit a ante commodo tristique</p>
                                                            </div>
                                                            <div className="ps-product__price"><span>$2.00</span></div>
                                                        </div>
                                                    </div>
                                                    <div className="ps-product--horizontal">
                                                        <div className="ps-product__thumbnail"><a className="ps-product__overlay" href="product-default.html" /><img src="template/user_chikery/img/product/18.png" alt="" /></div>
                                                        <div className="ps-product__container">
                                                            <div className="ps-product__content"><a className="ps-product__title" href="product-default.html">Red sugar flower</a>
                                                                <p>Nam ac elit a ante commodo tristique</p>
                                                            </div>
                                                            <div className="ps-product__price"><span>$5.00</span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="ps-section__right bg--cover" data-background="template/user_chikery/img/bg/home-1/home-menu-of-day.jpg" />
                    </div>
                    <div className="ps-section ps-home-product">
                        <div className="container-fluid">
                            <div className="ps-section__header">
                                <p>Breads every day</p>
                                <h3>Special Cake</h3><i className="chikery-tt3" />
                            </div>
                            <div className="ps-section__content">
                                <div className="row">
                                    <div className="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6 ">
                                        <div className="ps-product">
                                            <div className="ps-product__thumbnail"><img src="template/user_chikery/img/product/17.png" alt="" /><a className="ps-product__overlay" href="product-default.html" /><span className="ps-badge ps-badge--sale"><i>30%</i></span>
                                            </div>
                                            <div className="ps-product__content">
                                                <div className="ps-product__desc"><a className="ps-product__title" href="product-default.html">Red sugar flower</a>
                                                    <p><span>350g</span><span>30 Min</span><span>120 <sup>o</sup>C</span></p><span className="ps-product__price sale"><del>$16.00</del> $12.00</span>
                                                </div>
                                                <div className="ps-product__shopping"><a className="ps-btn ps-product__add-to-cart" href="#">Add to cart</a>
                                                    <div className="ps-product__actions"><a href="#"><i className="fa fa-heart-o" /></a><a href="#"><i className="fa fa-random" /></a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6 ">
                                        <div className="ps-product">
                                            <div className="ps-product__thumbnail"><img src="template/user_chikery/img/product/31.png" alt="" /><a className="ps-product__overlay" href="product-default.html" />
                                            </div>
                                            <div className="ps-product__content">
                                                <div className="ps-product__desc"><a className="ps-product__title" href="product-default.html">Red sugar flower</a>
                                                    <p><span>350g</span><span>30 Min</span><span>120 <sup>o</sup>C</span></p><span className="ps-product__price">$12.00</span>
                                                </div>
                                                <div className="ps-product__shopping"><a className="ps-btn ps-product__add-to-cart" href="#">Add to cart</a>
                                                    <div className="ps-product__actions"><a href="#"><i className="fa fa-heart-o" /></a><a href="#"><i className="fa fa-random" /></a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6 ">
                                        <div className="ps-product">
                                            <div className="ps-product__thumbnail"><img src="template/user_chikery/img/product/13.png" alt="" /><a className="ps-product__overlay" href="product-default.html" /><span className="ps-badge ps-badge--new"><i>New</i></span>
                                            </div>
                                            <div className="ps-product__content">
                                                <div className="ps-product__desc"><a className="ps-product__title" href="product-default.html">Red sugar flower</a>
                                                    <p><span>350g</span><span>30 Min</span><span>120 <sup>o</sup>C</span></p><span className="ps-product__price">$12.00</span>
                                                </div>
                                                <div className="ps-product__shopping"><a className="ps-btn ps-product__add-to-cart" href="#">Add to cart</a>
                                                    <div className="ps-product__actions"><a href="#"><i className="fa fa-heart-o" /></a><a href="#"><i className="fa fa-random" /></a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6 ">
                                        <div className="ps-product">
                                            <div className="ps-product__thumbnail"><img src="template/user_chikery/img/product/33.png" alt="" /><a className="ps-product__overlay" href="product-default.html" />
                                            </div>
                                            <div className="ps-product__content">
                                                <div className="ps-product__desc"><a className="ps-product__title" href="product-default.html">Red sugar flower</a>
                                                    <p><span>350g</span><span>30 Min</span><span>120 <sup>o</sup>C</span></p><span className="ps-product__price">$12.00</span>
                                                </div>
                                                <div className="ps-product__shopping"><a className="ps-btn ps-product__add-to-cart" href="#">Add to cart</a>
                                                    <div className="ps-product__actions"><a href="#"><i className="fa fa-heart-o" /></a><a href="#"><i className="fa fa-random" /></a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="ps-section__footer"><a className="ps-btn ps-btn--outline" href="#"> All products</a></div>
                        </div>
                    </div>
                    <div className="ps-section ps-testimonials bg--cover" data-background="template/user_chikery/img/bg/testimonials.jpg">
                        <div className="container">
                            <div className="ps-section__header">
                                <p>Testimonial</p>
                                <h3>What People Say</h3><i className="chikery-tt3" />
                            </div>
                            <div className="ps-section__content">
                                <div className="ps-carousel--dots-2 owl-slider" data-owl-auto="true" data-owl-loop="true" data-owl-speed={5000} data-owl-gap={0} data-owl-nav="false" data-owl-dots="true" data-owl-item={1} data-owl-item-xs={1} data-owl-item-sm={1} data-owl-item-md={1} data-owl-item-lg={1} data-owl-duration={1000} data-owl-mousedrag="on">
                                    <div className="ps-block--testimonial">
                                        <div className="ps-block__content">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sed turpis feugiat, mollis felis vel, viverra metus. Sed vel nulla non neque dictum imperdiet. Aliquam egestas hendrerit euismod.</p>
                                        </div>
                                        <div className="ps-block__footer">
                                            <p>Marry Lulie</p>
                                        </div>
                                    </div>
                                    <div className="ps-block--testimonial">
                                        <div className="ps-block__content">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sed turpis feugiat, mollis felis vel, viverra metus. Sed vel nulla non neque dictum imperdiet. Aliquam egestas hendrerit euismod.</p>
                                        </div>
                                        <div className="ps-block__footer">
                                            <p>Harold M. Clark</p>
                                        </div>
                                    </div>
                                    <div className="ps-block--testimonial">
                                        <div className="ps-block__content">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sed turpis feugiat, mollis felis vel, viverra metus. Sed vel nulla non neque dictum imperdiet. Aliquam egestas hendrerit euismod.</p>
                                        </div>
                                        <div className="ps-block__footer">
                                            <p>Timothy A. Mitchell</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="ps-home-book-table">
                        <div className="ps-section__left bg--cover" data-background="template/user_chikery/img/section/book-your-tables/bg.jpg">
                            <div className="ps-section__content">
                                <p>Video Present</p>
                                <h3>Any Design <br /> For Your Feast-day</h3>
                                <div className="ps-video"><a href="https://www.youtube.com/watch?v=2_g48Zw0rao"><i className="fa fa-play" /></a></div>
                            </div>
                        </div>
                        <div className="ps-section__right">
                            <form className="ps-form--book-your-table" action="http://nouthemes.net/html/chikery/index.html" method="get">
                                <div className="ps-form__header">
                                    <h4>Chikery Cake</h4>
                                    <h3>Book your table</h3><i className="chikery-tt3" />
                                    <p>Book your table now online and get a confirmation in second. For bar and terrace booking, please contact us directly via<span> (+334) 3445445</span></p>
                                </div>
                                <div className="ps-form__content">
                                    <div className="row">
                                        <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                                            <div className="form-group">
                                                <input className="form-control" type="text" placeholder="Name" />
                                            </div>
                                        </div>
                                        <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                                            <div className="form-group">
                                                <input className="form-control" type="text" placeholder="Phone" />
                                            </div>
                                        </div>
                                        <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                                            <div className="form-group">
                                                <input className="form-control" type="email" placeholder="Email" />
                                            </div>
                                        </div>
                                        <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                                            <div className="form-group">
                                                <input className="form-control" type="text" placeholder="Number of People" />
                                            </div>
                                        </div>
                                        <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                                            <div className="form-group">
                                                <input className="form-control" type="text" placeholder="Date" />
                                            </div>
                                        </div>
                                        <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                                            <div className="form-group">
                                                <input className="form-control" type="text" placeholder="Time" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="ps-form__footer">
                                    <button className="ps-btn">Book Now</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <footer className="ps-footer">
                    <div className="ps-footer__content">
                        <div className="container">
                            <div className="ps-footer__left">
                                <form className="ps-form--footer-subscribe" action="http://nouthemes.net/html/chikery/index.html" method="get">
                                    <h3>Get news &amp; offer</h3>
                                    <p>Sign up for our mailing list to get latest update and offers</p>
                                    <div className="form-group--inside">
                                        <input className="form-control" type="text" placeholder="Your email..." />
                                        <button><i className="fa fa-arrow-right" /></button>
                                    </div>
                                    <p>* Don't worry, we never spam</p>
                                </form>
                            </div>
                            <div className="ps-footer__center">
                                <div className="ps-site-info"><a className="ps-logo" href="#"><img src="template/user_chikery/img/logo.png" alt="" /></a>
                                    <p>004 Riley Street, Surry Hills 2050 Sydney, Australia</p>
                                    <p>Email:<a href="#"> <span className="__cf_email__" data-cfemail="244d4a424b644a4b51504c414941570a474b49">[email&nbsp;protected]</span></a></p>
                                    <p>Phone:<span className="ps-hightlight"> +455 45 454555</span></p>
                                </div>
                            </div>
                            <div className="ps-footer__right">
                                <aside className="widget widget_footer">
                                    <h3 className="widget-title">Opening Time</h3>
                                    <p>Monday - Friday:  <span>08:00 am - 08:30 pm</span></p>
                                    <p>Saturday - Sunday:  <span>10:00 am - 16:30 pm</span></p>
                                    <ul className="ps-list--payment-method">
                                        <li><a href="#"><i className="fa fa-cc-mastercard" /></a></li>
                                        <li><a href="#"><i className="fa fa-cc-visa" /></a></li>
                                        <li><a href="#"><i className="fa fa-cc-paypal" /></a></li>
                                        <li><a href="#"><i className="fa fa-cc-discover" /></a></li>
                                    </ul>
                                </aside>
                            </div>
                        </div>
                    </div>
                    <div className="ps-footer__bottom">
                        <div className="container">
                            <ul className="ps-list--social">
                                <li><a href="#"><i className="fa fa-facebook" /></a></li>
                                <li><a href="#"><i className="fa fa-twitter" /></a></li>
                                <li><a href="#"><i className="fa fa-google-plus" /></a></li>
                                <li><a href="#"><i className="fa fa-instagram" /></a></li>
                                <li><a href="#"><i className="fa fa-youtube-play" /></a></li>
                            </ul>
                            <p>© 2019 Chikery.  Made by<a href="#"> Nouthemes</a></p>
                        </div>
                    </div>
                </footer>
                <Subscribe/>
                <div id="back2top"><i className="pe-7s-angle-up" /></div>
                <div className="ps-site-overlay" />
                <div id="loader-wrapper">
                    <div className="loader-section section-left" />
                    <div className="loader-section section-right" />
                </div>
                <div className="ps-search" id="site-search"><a className="ps-btn--close" href="#" />
                    <div className="ps-search__content">
                        <form className="ps-form--primary-search" action="http://nouthemes.net/html/chikery/do_action" method="post">
                            <input className="form-control" type="text" placeholder="Search for..." />
                            <button><i className="fa fa-search" /></button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default HomePage;