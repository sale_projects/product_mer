import React from 'react';

class Subscribe extends React.Component {
    render() {
        return (
            <div className="ps-popup" id="subscribe" data-time={500}>
                <div className="ps-popup__content bg--cover"
                     data-background="template/user_chikery/img/bg/subcribe.jpg"><a
                    className="ps-popup__close ps-btn--close" href="#"/>
                    <form className="ps-form--subscribe-popup" action="http://nouthemes.net/html/chikery/index.html"
                          method="get">
                        <div className="ps-form__content">
                            <h5>Newletters</h5>
                            <h3>Get Discount <span>30%</span> Off</h3>
                            <p>Sign up to our newsletter and save 30% for you next purchase. No spam, we promise !</p>
                            <div className="form-group">
                                <input className="form-control" type="text" placeholder="Email Address" required/>
                                <button className="ps-btn">Subscribe</button>
                            </div>
                            <div className="ps-checkbox ps-checkbox--circle">
                                <input className="form-control" type="checkbox" id="not-show" name="not-show"/>
                                <label htmlFor="not-show">Don't show this popup again</label>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

export default Subscribe;