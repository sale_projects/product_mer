import React from 'react';

class Header extends React.Component {
    render() {
        return (
            <div>
                <header className="header header--1" data-sticky="true">
                    <div className="header__left"><a className="menu-toggle ps-modal-link"
                                                     href="#menu-fullscreen"><span/></a>
                        <p><i className="fa fa-clock-o"/> 08:00 am - 08:30 pm</p>
                    </div>
                    <div className="header__center"><a className="ps-logo" href="#"><img
                        src="template/user_chikery/img/logo.png" alt=""/></a></div>
                    <div className="header__right">
                        <div className="header__actions"><a className="ps-search-btn" href="#"><i
                            className="fa fa-search"/></a><a
                            href="#"><i className="fa fa-heart-o"/></a>
                            <div className="ps-cart--mini"><a className="ps-cart__toggle" href="#"><i
                                className="fa fa-shopping-basket"/><span><i>2</i></span></a>
                                <div className="ps-cart__content">
                                    <div className="ps-cart__items">
                                        <div className="ps-product--mini-cart">
                                            <div className="ps-product__thumbnail"><a href="#"><img
                                                src="template/user_chikery/img/product/12.png" alt=""/></a></div>
                                            <div className="ps-product__content"><span className="ps-btn--close"/><a
                                                className="ps-product__title" href="product-default.html">Jean Woman
                                                Summer</a>
                                                <p><strong>Quantity: 1</strong></p><small>$12.00</small>
                                            </div>
                                        </div>
                                        <div className="ps-product--mini-cart">
                                            <div className="ps-product__thumbnail"><a href="#"><img
                                                src="template/user_chikery/img/product/13.png" alt=""/></a></div>
                                            <div className="ps-product__content"><span className="ps-btn--close"/><a
                                                className="ps-product__title" href="product-default.html">Jean Woman
                                                Summer</a>
                                                <p><strong>Quantity: 1</strong></p><small>$12.00</small>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="ps-cart__footer">
                                        <h3>Sub Total:<strong>$48.00</strong></h3>
                                        <figure><a className="ps-btn" href="shopping-cart.html">View Cart</a><a
                                            className="ps-btn ps-btn--dark" href="checkout.html">Checkout</a></figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <div className="ps-modal" id="menu-fullscreen">
                    <div className="header--fullscreen ps-modal__container bg--cover"
                         data-background="template/user_chikery/img/bg/menu-bg.jpg"><a
                        className="ps-modal__close ps-btn--close" href="#"/>
                        <div className="header__top"><a className="ps-logo" href="#"><img
                            src="template/user_chikery/img/logo.png" alt=""/></a></div>
                        <div className="header__content">
                            <ul className="menu--fullscreen">
                                <li className="current-menu-item "><a href="#">Home</a>
                                </li>
                                <li><a href="about-us.html">About</a>
                                </li>
                                <li><a href="about-us.html">Pages</a>
                                </li>
                                <li><a href="blog-grid.html">News</a>
                                </li>
                                <li><a href="contact-us.html">Contact</a>
                                </li>
                            </ul>
                        </div>
                        <div className="header__bottom">
                            <ul className="ps-list--social">
                                <li><a href="#"><i className="fa fa-facebook"/></a></li>
                                <li><a href="#"><i className="fa fa-twitter"/></a></li>
                                <li><a href="#"><i className="fa fa-google-plus"/></a></li>
                                <li><a href="#"><i className="fa fa-instagram"/></a></li>
                                <li><a href="#"><i className="fa fa-youtube-play"/></a></li>
                            </ul>
                            <p>© 2019 Chikery. Made by<a href="#"> Nouthemes</a></p>
                        </div>
                    </div>
                </div>
                <header className="header header--mobile" data-sticky="false">
                    <div className="header__content">
                        <div className="header__left"><a className="ps-toggle--sidebar" href="#navigation-mobile"><i
                            className="fa fa-bars"/></a></div>
                        <div className="header__center"><a className="ps-logo" href="#"><img
                            src="template/user_chikery/img/logo.png" alt=""/></a></div>
                        <div className="header__right">
                            <div className="header__actions"><a href="#"><i className="fa fa-heart-o"/></a></div>
                        </div>
                    </div>
                </header>
            </div>
        );
    }
}

export default Header;